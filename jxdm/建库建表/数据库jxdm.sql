##建库建表

    DROP DATABASE IF EXISTS jxdm;
    CREATE DATABASE jxdm CHARSET=UTF8;
    USE jxdm;
    CREATE TABLE user(id INT PRIMARY KEY AUTO_INCREMENT,
                      username VARCHAR(50),
                      password VARCHAR(50),
                      nickname VARCHAR(50),
                      age INT(2),
                      gender VARCHAR(10),
                      email VARCHAR(50),
                      phone VARCHAR(50),
                      created TIMESTAMP
    );

    CREATE TABLE posts(id INT PRIMARY KEY AUTO_INCREMENT,content VARCHAR(255),urls TEXT,created TIMESTAMP,user_id INT);

    CREATE TABLE comment(id INT PRIMARY KEY AUTO_INCREMENT,content VARCHAR(255),created TIMESTAMP,user_id INT,posts_id INT);

    CREATE TABLE category(id INT PRIMARY KEY AUTO_INCREMENT,name VARCHAR(10));
    INSERT INTO category VALUES(NULL,'猫粮'),(NULL,'医疗护理'),(NULL,'保健'),(NULL,'猫砂猫厕');

    CREATE TABLE banner(id INT PRIMARY KEY AUTO_INCREMENT,url VARCHAR(255));
    INSERT INTO banner VALUES(NULL,'/catimg/yinjianc1.jpg'),(NULL,'/catimg/jinjianc2.jpg'),(NULL,'/catimg/jinjianc1.jpg'),(NULL,'/catimg/meiduan1.jpg');

    CREATE TABLE product(id INT PRIMARY KEY AUTO_INCREMENT,title VARCHAR(50),
                     price DOUBLE(10,2),old_price DOUBLE(10,2),sale_count INT,
                     num INT,category_id INT,url VARCHAR(255),view_count INT,
                     created TIMESTAMP);
    INSERT INTO product VALUES (NULL, '猫粮(演示商品)', 199.98, 299.98, 2000, 99999, 1, '/catimg/yinjianc1.jpg', 0, '2023-03-10 17:26:50'),
                               (NULL, '医疗护理商品(猫医疗护理)', 531.08, 691.08, 2000, 99999, 2, '/catimg/yinjianc2.jpg', 0, '2023-03-10 17:26:50'),
                               (NULL, '猫猫保健(演示商品)', 600.01, 809.76, 2000, 99999, 3, '/catimg/meiduan1.jpg', 0, '2023-03-10 17:26:50'),
                               (NULL, '猫砂猫厕(演示商品)', 399.09, 500.81, 2000, 99999, 4, '/catimg/meiduan2.jpg', 0, '2023-03-10 17:26:50'),
                               (NULL, '猫粮商品猫粮', 109.08, 299.98, 42100, 99999, 1, '/catimg/jumao1.jpg', 0, '2023-03-10 17:26:50'),
                               (NULL, '猫猫医疗护理', 231.8, 691.08, 32200, 99999, 2, '/catimg/jumao2.jpg', 0, '2023-03-10 17:26:50'),
                               (NULL, '保健商品', 300.01, 809.76, 22300, 99999, 3, '/catimg/bizhi7.jpg', 0, '2023-03-10 17:26:50'),
                               (NULL, 'xx牌猫砂', 409.00, 500.81, 12400, 99999, 4, '/catimg/bizhi6.jpg', 0, '2023-03-10 17:26:50');

    create table notice(
        id int primary key auto_increment,
        title varchar(255),
        content varchar(255),
        created timestamp
    );
    insert into notice values (NULL,'猫砂的选择','市场上的带砂大最可以分为些类土、木幕、豆南猫破、水昌管砂，各牌带砂都有各自的优缺点。',null),
                              (NULL,'关于猫粮好坏的分析','所以对于不问的猫主人说 要根据自己的需求选择适合自己的猫砂。',null),
                              (NULL,'产品页面显示在维护中，应该如何预订','我建议每个铲屎官重点了解以下几个重要指标，记住现住，猫是食肉肉动物!!!',null),
                              (NULL,'街巷家的产品一般要提前多久预订','别只看一些胡萝卜成分啊。南瓜成分啊,一定要仔细看!',null),
                              (NULL,'如何预订产品','门票类产品仅限二维码及手机客户端预订，不接受电话预订。具体请查看产品中的预订限制为准',null),
                              (NULL,'线下猫咖体验怎么预订','请在订单提交后与工作人员联系客人确认,以及相关报价,建议在预订时看下是否有限制疫情出行的相关限制',null);



