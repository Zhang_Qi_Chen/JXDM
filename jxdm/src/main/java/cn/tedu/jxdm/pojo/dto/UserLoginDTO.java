package cn.tedu.jxdm.pojo.dto;

import lombok.Data;

@Data
public class UserLoginDTO {
    private String username;
    private String password;

}
