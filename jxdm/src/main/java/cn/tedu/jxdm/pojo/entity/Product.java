package cn.tedu.jxdm.pojo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Product {
    private Integer id;
    private String title;
    private Double price;
    private Double oldPrice;
    private Integer saleCount;
    private Integer num;
    private Integer categoryId;
    private String url;
    private Integer viewCount;
    private Date created;

}
