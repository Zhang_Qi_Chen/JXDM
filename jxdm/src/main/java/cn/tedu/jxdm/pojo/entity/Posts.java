package cn.tedu.jxdm.pojo.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Posts {
    private Integer id;
    private String content;
    private String urls;
    private Date created;
    private Integer userId;  //user_id

}
