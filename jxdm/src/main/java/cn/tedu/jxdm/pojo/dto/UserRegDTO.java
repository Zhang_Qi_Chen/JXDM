package cn.tedu.jxdm.pojo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRegDTO implements Serializable {


    private String username;
    private String password;
    private String nickname;
    private Integer age;
    private String gender;
    private String email;
    private String phone;

}
