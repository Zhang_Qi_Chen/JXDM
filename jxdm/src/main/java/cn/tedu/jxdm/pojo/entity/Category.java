package cn.tedu.jxdm.pojo.entity;

import lombok.Data;

@Data
public class Category {
    private Integer id;
    private String name;

}
