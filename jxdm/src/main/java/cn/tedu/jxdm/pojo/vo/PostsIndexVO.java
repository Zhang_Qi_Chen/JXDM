package cn.tedu.jxdm.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PostsIndexVO implements Serializable {
    private Integer id;
    private String content;
    private String nickname;

}
