package cn.tedu.jxdm.pojo.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductIndexVO implements Serializable {
    private Integer id;
    private String title;
    private Double price;
    private Double oldPrice;
    private Integer saleCount;
    private String url;
}
