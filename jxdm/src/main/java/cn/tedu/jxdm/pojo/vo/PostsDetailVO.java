package cn.tedu.jxdm.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class PostsDetailVO {
    private Integer id;
    private String content;
    private String urls;
//    2023年3月10号 11点22分30秒
    @JsonFormat(pattern = "yyyy年MM月dd号 HH点mm分ss秒",timezone = "GMT+8")
    private Date created;
    private String nickname;

}
