package cn.tedu.jxdm.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.jxdm.mapper")
public class Config {
}
