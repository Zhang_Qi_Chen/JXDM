package cn.tedu.jxdm.web;

import cn.tedu.jxdm.ex.ServiceException;
import lombok.Data;

import java.io.Serializable;

/**
 * 统一响应结果类型
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Data
public class JsonResult implements Serializable {

    /**
     * 状态码
     */
    private Integer state;
    /**
     * 操作“失败”时的描述文本
     */
    private String message;
    /**
     * 操作“成功”时响应的数据
     */
    private Object data;

    public static JsonResult ok() {
        // JsonResult jsonResult = new JsonResult();
        // jsonResult.setState(ServiceCode.OK.getValue());
        // return jsonResult;
        return ok(null);
    }

    public static JsonResult ok(Object data) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setState(ServiceCode.OK.getValue());
        jsonResult.setData(data);
        return jsonResult;
    }

    public static JsonResult fail(ServiceException e) {
        // JsonResult jsonResult = new JsonResult();
        // jsonResult.setState(e.getState());
        // jsonResult.setMessage(e.getMessage());
        // return jsonResult;
        return fail(e.getServiceCode(), e.getMessage());
    }

    public static JsonResult fail(ServiceCode serviceCode, String message) {
        JsonResult jsonResult = new JsonResult();
        jsonResult.setState(serviceCode.getValue());
        jsonResult.setMessage(message);
        return jsonResult;
    }

}

