package cn.tedu.jxdm.controller;


import cn.tedu.jxdm.mapper.UserMapper;
import cn.tedu.jxdm.pojo.dto.UserLoginDTO;
import cn.tedu.jxdm.pojo.dto.UserRegDTO;
import cn.tedu.jxdm.pojo.entity.User;
import cn.tedu.jxdm.pojo.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;


@RestController
@Validated
public class UserController {
    @Autowired(required = false)
    UserMapper mapper;
    @RequestMapping("/reg")
    public int reg(@RequestBody UserRegDTO userRegDTO){
        //看看前端传来的注册数据
        System.out.println("前端传来的注册数据userRegDTO = " + userRegDTO);
        //1.看看用户名注册过吗,给前端返回值response  1或者2
        UserVO userVO = mapper.selectByUserName(userRegDTO.getUsername());
        if (userVO!=null){
            return 2;//代表用户名已存在!
        }
        User u = new User();
        BeanUtils.copyProperties(userRegDTO,u);//把dto里面已有的属性(7条用户注册信息)复制到实体类中
        u.setCreated(new Date());//user实体类中加上创建时间
        mapper.insert(u);
        return 1;
    }

    @RequestMapping("/login")
    public int login(@RequestBody UserLoginDTO userLoginDTO, HttpSession session){
        UserVO userVO = mapper.selectByUserName(userLoginDTO.getUsername());
        if (userVO!=null){
            if (userVO.getPassword().equals(userLoginDTO.getPassword())){
                //把登录成功的UserVO保存到 当前客户端对应的会话对象里面
                session.setAttribute("user",userVO);
                return 1;
            }
            return 2;
        }
        return 3;
    }
    @RequestMapping("/currentUser")
    public UserVO currentUser(HttpSession session){
        //从当前客户端对应的会话对象里面取出登录的UserVO
        UserVO user = (UserVO) session.getAttribute("user");
        //如果登陆过返回user对象 未登录返回null  但是经过JSON字符串转换后到客户端为空字符串
        return user;
    }
    @RequestMapping("/logout")
    public void logout(HttpSession session){
        session.removeAttribute("user");
    }
}
