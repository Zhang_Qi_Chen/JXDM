package cn.tedu.jxdm.controller;

import cn.tedu.jxdm.mapper.CommentMapper;
import cn.tedu.jxdm.pojo.dto.CommentDTO;
import cn.tedu.jxdm.pojo.entity.Comment;
import cn.tedu.jxdm.pojo.vo.CommentVO;
import cn.tedu.jxdm.pojo.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/comment/")
public class CommentController {
    @Autowired(required = false)
    CommentMapper mapper;

    @RequestMapping("insert")
    public int insert(@RequestBody CommentDTO commentDTO, HttpSession session) {
        UserVO userVO = (UserVO) session.getAttribute("user");
        if (userVO == null) {
            return 2;
        }
        Comment c = new Comment();
        BeanUtils.copyProperties(commentDTO, c);
        c.setCreated(new Date());
        c.setUserId(userVO.getId());
        mapper.insert(c);
        return 1;
    }

    @RequestMapping("selectByPostsId")
    public List<CommentVO> selectByPostsId(int id) {

        return mapper.selectByPostsId(id);
    }
}
