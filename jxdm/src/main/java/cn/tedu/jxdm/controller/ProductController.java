package cn.tedu.jxdm.controller;

import cn.tedu.jxdm.pojo.dto.ProductDTO;
import cn.tedu.jxdm.pojo.vo.ProductDetailVO;
import cn.tedu.jxdm.pojo.vo.ProductIndexVO;
import cn.tedu.jxdm.service.IProductService;
import cn.tedu.jxdm.web.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/product/")
@Validated
public class ProductController {
    //给变量赋值
    @Value("${imagesPath}")
    private String dirPath;
    @Autowired
    IProductService productService;

    @RequestMapping("insert")
    public JsonResult insert(@RequestBody ProductDTO productDTO) {
        productService.insert(productDTO);
        return JsonResult.ok();
    }

    @RequestMapping("selectDetailById")
    public JsonResult selectDetailById(int id, HttpSession session) {
        ProductDetailVO productDetailVO = productService.selectDetailById(id, session);
        return JsonResult.ok(productDetailVO);
    }

    //首页商品展现
    @RequestMapping("selectIndex")
    public JsonResult selectIndex() {
        List<ProductIndexVO> productIndexVOS = productService.selectIndex();
        return JsonResult.ok(productIndexVOS);
    }

    @RequestMapping("selectByWd")
    public JsonResult selectByWd(String wd) {
        List<ProductIndexVO> productIndexVOS = productService.selectByWd(wd);
        return JsonResult.ok(productIndexVOS);
    }

    @RequestMapping("selectByCid")
    public JsonResult selectByCid(int id) {
        List<ProductIndexVO> productIndexVOS = productService.selectByCid(id);
        return JsonResult.ok(productIndexVOS);
    }
}
