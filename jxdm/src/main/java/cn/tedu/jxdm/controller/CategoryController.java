package cn.tedu.jxdm.controller;

import cn.tedu.jxdm.mapper.CategoryMapper;
import cn.tedu.jxdm.pojo.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/category/")
public class CategoryController {
    @Autowired
    CategoryMapper mapper;

    @RequestMapping("select")
    public List<Category>select(){
        return mapper.select();
    }
}
