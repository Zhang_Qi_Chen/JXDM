package cn.tedu.jxdm.mapper;

import cn.tedu.jxdm.pojo.entity.Product;
import cn.tedu.jxdm.pojo.vo.ProductDetailVO;
import cn.tedu.jxdm.pojo.vo.ProductIndexVO;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProductMapper {
    int updateViewCountById(int id);


    ProductDetailVO selectDetailById(int id);

    List<ProductIndexVO> selectIndex();

    int insert(Product p);

    List<ProductIndexVO> selectByWd(String wd);

    List<ProductIndexVO> selectByCid(int id);
}
