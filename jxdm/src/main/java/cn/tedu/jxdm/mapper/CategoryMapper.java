package cn.tedu.jxdm.mapper;

import cn.tedu.jxdm.pojo.entity.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


public interface CategoryMapper {
    List<Category> select();
}
