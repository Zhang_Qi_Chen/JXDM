package cn.tedu.jxdm.mapper;

import cn.tedu.jxdm.pojo.entity.Comment;
import cn.tedu.jxdm.pojo.vo.CommentVO;

import java.util.List;

public interface CommentMapper {
    void insert(Comment comment);

    List<CommentVO> selectByPostsId(int id);
}
