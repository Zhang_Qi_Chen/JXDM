package cn.tedu.jxdm.mapper;

import cn.tedu.jxdm.pojo.entity.Posts;
import cn.tedu.jxdm.pojo.vo.PostsDetailVO;
import cn.tedu.jxdm.pojo.vo.PostsIndexVO;

import java.util.List;

public interface PostsMapper {
    void insert(Posts weibo);

    List<PostsIndexVO> selectIndex();

    PostsDetailVO selectById(int id);
}
