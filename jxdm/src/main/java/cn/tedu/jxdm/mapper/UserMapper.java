package cn.tedu.jxdm.mapper;


import cn.tedu.jxdm.pojo.entity.User;
import cn.tedu.jxdm.pojo.vo.UserVO;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    UserVO selectByUserName(String username);//根据username 查询用户名
    int insert(User user);//添加用户

    int countByName(String name);

    UserVO selectByPassword(String password);

}
