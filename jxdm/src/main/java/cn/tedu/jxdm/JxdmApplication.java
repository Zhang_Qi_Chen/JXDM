package cn.tedu.jxdm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JxdmApplication {

    public static void main(String[] args) {
        SpringApplication.run(JxdmApplication.class, args);
    }

}
