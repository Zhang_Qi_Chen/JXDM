package cn.tedu.jxdm.ex;

import cn.tedu.jxdm.web.ServiceCode;
import lombok.Getter;

public class ServiceException extends RuntimeException {
    @Getter
    private ServiceCode serviceCode;

    public ServiceException(ServiceCode serviceCode, String message) {
        super(message);
        this.serviceCode=serviceCode;
    }
}
