package cn.tedu.jxdm.service;

import cn.tedu.jxdm.pojo.dto.UserRegDTO;
import cn.tedu.jxdm.pojo.vo.UserVO;

/**
 * 处理注册和登录的业务接口
 */
public interface IUserService {
    //查询用户名
    UserVO selectByUserName(String username);

    //添加用户
    void insert(UserRegDTO userRegDTO);

    UserVO selectByPassword(String password);

}
