package cn.tedu.jxdm.service;

import cn.tedu.jxdm.pojo.dto.ProductDTO;
import cn.tedu.jxdm.pojo.vo.ProductDetailVO;
import cn.tedu.jxdm.pojo.vo.ProductIndexVO;
import org.apache.ibatis.annotations.Mapper;

import javax.servlet.http.HttpSession;
import java.util.List;


public interface IProductService {
    /**
     *
     * @param productDTO
     */
    void insert(ProductDTO productDTO);

    /**
     *
     * @param id
     * @param session
     * @return
     */
    ProductDetailVO selectDetailById(int id, HttpSession session);

    /**
     *
     * @return
     */
    List<ProductIndexVO> selectIndex();

    /**
     *
     * @param wd
     * @return
     */
    List<ProductIndexVO> selectByWd(String wd);

    /**
     *
     * @param id
     * @return
     */
    List<ProductIndexVO> selectByCid(int id);
}
